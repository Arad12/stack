﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stack
{
    class Program
    {
        public static int GetTopSum(Stack<int>[] stacks)
        {
            // this action gets a stack's array and return the sum of all the first numbers in each stack
            int sum = 0;
            foreach(Stack<int> part in stacks)
                if(part.Count()>0)
                sum += part.Peek();
            return sum;
        }

        public static int RemoveItemAt( Stack<int> stack, int num)
        {
            // this function get a stack and a number.
            // it pop out the value at the 'num' sit and return it. the stack lose the value at the 'num' sit. 
            int number = 0;
            Stack<int> help = new Stack<int>();
            if (stack.Count() >= num)
            {
                for (int i = 0; i < num -1; i++)
                {
                    help.Push(stack.Pop());
                }
                number = stack.Pop();
                for (int i = 0; i <= help.Count + 1; i++)
                {
                    stack.Push(help.Pop());
                }
            }
             return number;
        }
        
        public static bool IsBracketBalanced (string str)
        {
            // this function check if an arithmetic expression is ok
            Stack<char> open = new Stack<char>();
            foreach (char x in str)
            {
                if ((x.ToString().Equals("(")) || (x.ToString().Equals("[")) || (x.ToString().Equals("{")))
                    open.Push(x);

                if (open.Count() > 0)
                {
                    if (open.Peek().ToString().Equals("(") && x.ToString().Equals(")"))
                        open.Pop();
                    else if (open.Peek().ToString().Equals("[") && x.ToString().Equals("]"))
                        open.Pop();
                    else if (open.Peek().ToString().Equals("{") && x.ToString().Equals("}"))
                        open.Pop();
                }
            }
            if (open.Count() == 0)
                return true;

            return false;
        }
        


        static void Main(string[] args)
        {
            Stack<int> a = new Stack<int>();
            a.Push(1);
            a.Push(2);
            a.Push(3);
            a.Push(4);
            a.Push(5);
            a.Push(6);



        }
    }
}
